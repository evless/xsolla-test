module.exports = {
    plugins: [
        require('precss'),
        require('autoprefixer'),
        require('postcss-simple-vars'),
        require('postcss-calc'),
        require('postcss-automath'),
        require('postcss-nested'),
        require('postcss-mixins')
    ]
}