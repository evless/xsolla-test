import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory, Route, IndexRoute, browserHistory, Redirect, IndexRedirect } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import 'whatwg-fetch';

import Content from './containers/content/content.jsx';
import Users from './containers/users/users.jsx';
import NotFound from './containers/notFound/notFound.jsx';
import UserView from './containers/userView/userView.jsx';
import UserCreate from './containers/userCreate/userCreate.jsx';

import '../public/styles/index.css';

// Store
import configureStore from './store/configureStore';
const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path='/' component={Content}>
                <IndexRedirect to='users' />
                <Route path='users' title='Список пользователей' component={Users}></Route>
                <Route path='create' title='Создание пользователя' component={UserCreate}></Route>
                <Route path='user'>
                    <Route path=':userID'>
                        <Route path='view' title='Карточка пользователя' component={UserView}/>
                    </Route>
                </Route>
            </Route>
            <Route path='*' title='Страница не найдена' component={NotFound} />
        </Router>
    </Provider>,
    document.getElementById('body')
);