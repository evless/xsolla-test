import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './button.css';

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {...props};
    }

    handler = (event) => {
        this.props.onClickHandler(event.target.value);
    }

    render() {
        const { title, disabled } = this.props;

        return (
            <field className="form-field">
                <button className="btn" type="submit" onClick={this.handler} disabled={disabled}>
                    {title}
                </button>
            </field>
        )
    }
}

Button.propTypes = {
    onClickHandler: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired
}

export default Button;