import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Button from './button.jsx';

describe('Компонент Button', () => {
    const testHandler = function(newValue) {
        return true
    }

    const button = shallow(<Button title="Тестовая кнопка" disabled={false} onClickHandler={testHandler} />);

    it('Проверка есть ли кнопка', () => {
        expect(button.find('button')).to.have.length(1);
    });

    it('Клик по кнопке', () => {
        expect(button.state().onClickHandler()).to.equal(true);
    });
});