import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './input.css';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {...props};
    }

    handler = (event) => {
        this.props.onChangeHandler(event.target.value);
    }

    render() {
        const { value, title, disabled, error } = this.props;

        return (
            <field className="form-field">
                <input type="text" placeholder={title} onChange={this.handler} value={value} disabled={disabled} />
                {
                    error
                    ? <div className="form-field__error">{error[0]}</div>
                    : null
                }
            </field>
        )
    }
}

Input.propTypes = {
    onChangeHandler: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    error: PropTypes.array
}

export default Input;