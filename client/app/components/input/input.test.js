import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Input from './input.jsx';

describe('Компонент Input', () => {
    let value = 'Тест';
    const testHandler = function(newValue) {
        value = newValue
        input.setState({value: newValue})
    }

    const input = shallow(<Input title="Тестовый инпут" value={value} onChangeHandler={testHandler} />);

    it('Проверка есть ли инпут', () => {
        expect(input.find('input')).to.have.length(1);
    });

    it('Изменение значения', () => {
        expect(input.state().value).to.equal(value);
        input.state().onChangeHandler('Тест2');
        input.update();
        expect(input.state().value).to.equal(value);
    });

    it('Вывод ошибки', () => {
        input.setProps({error: ['Тестовая ошибка']});
        input.update();
        expect(input.find('.form-field__error')).to.have.length(1);
    })
});