import React, { Component } from 'react';
import PropTypes from 'prop-types';

import NumberFormat from 'react-number-format';

class Amount extends Component {
    constructor(props) {
        super(props);
        this.state = {...props};
    }

    render() {
        let { value } = this.props;

        if (undefined !== value) {
            value = value.toFixed(2)
        } else {
            return null;
        }

        return (
            <NumberFormat value={value}
                displayType={'text'}
                prefix={'$'}
                thousandSeparator={true}
                thousandSeparator={' '}
                decimalSeparator={'.'} />
        )
    }
}

Amount.propTypes = {
    value: PropTypes.number
}

export default Amount;