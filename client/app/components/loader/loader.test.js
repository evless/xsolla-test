import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Loader from './loader.jsx';

describe('Компонент Loader', () => {
    const loader = shallow(<Loader text="Загрузка" loading={false}/>);

    it('Проверка есть ли кнопка', () => {
        expect(loader.find('.loader-background')).to.have.length(0);
    });

    it('Клик по кнопке', () => {
        loader.setProps({loading: true})
        expect(loader.find('.loader-background')).to.have.length(1);
        expect(loader.find('.loader-text').text()).to.equal('Загрузка');
    });
});