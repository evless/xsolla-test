import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './loader.css';

class Loader extends Component {
    constructor(props) {
        super(props);
        this.state = {...props};
    }
    render() {
        if (!this.props.loading) return null;
        return (
            <div className="loader-background">
                <div className="loader"></div>
                <div className="loader-text">{this.state.text}</div>
            </div>
        );
    }
}

Loader.propTypes = {
    loading: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
}

export default Loader;