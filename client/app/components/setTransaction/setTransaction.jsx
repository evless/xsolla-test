import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

import Input from '../input/input.jsx';
import Button from '../button/button.jsx';
import Loader from '../loader/loader.jsx';

import './setTransaction.css';

import { transaction } from '../../constans/transaction.js';

const customStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        transform: 'translate(-50%, -50%)',
        overflow: 'initial'
    }
}

class SetTransaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props,
            loading: false,
            transaction,
            disabledForm: false,
            buttonText: 'Добавить',
            disabledSubmit: true,
            errors: {},
            success: false,
            isClosedButton: true
        };
    }

    /**
     * Проверка нужно или нет задизейблить кнопку.
     */
    checkDisabled() {
        let { transaction } = this.state;

        if (0 < transaction.amount.length && 0 < transaction.comment.length) {
            this.setState({
                disabledSubmit: false
            })
        } else {
            this.setState({
                disabledSubmit: true
            })
        }
    }

    /**
     * 
     * Общий обработчик для всех инпутов.
     * 
     * @param {String} type -> Имя поле в объекте, куда нужно сложить данные
     * @param {String} value -> Новое значение в инпуте
     */
    handler(type, value) {
        this.setState({
            transaction: {
                ...this.state.transaction,
                [type]: value
            }
        }, this.checkDisabled)
    }

    submit = () => {
        let { userID } = this.state;

        let options = {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(this.state.transaction)
        }

        this.setState({
            loading: true,
            disabledForm: true,
            errors: {}
        }, () => {
            fetch(`https://livedemo.xsolla.com/fe/test-task/baev/users/${userID}/recharge`, options)
                .then(res => res.json())
                .then(res => {
                    if (res.amount) {
                        // Убираем лоадер и меняем текст в кнопке
                        this.setState({
                            buttonText: 'Транзакция создана',
                            disabledSubmit: true,
                            loading: false,
                            success: true,
                            isClosedButton: false
                        }, () => {
                            // Потом возвращаем всё в исходное положение
                            setTimeout(() => {
                                this.setState({
                                    transaction,
                                    buttonText: 'Добавить',
                                    disabledForm: false,
                                    isClosedButton: true
                                }, this.onClosedModalHandler)
                            }, 2000)
                        })
                    } else {
                        let errors = res.extended_message.property_errors;

                        this.setState({
                            loading: false,
                            disabledForm: false,
                            errors
                        })
                    }
                })
        })
    }

    /**
     * При закрытии модалки прокидываем успешность создания транзакции, что бы обновить их список
     */
    onClosedModalHandler = () => {
        this.props.onClosedModal(this.state.success);
    }

    render() {
        const { transaction, disabledForm, buttonText, disabledSubmit, errors, loading, isClosedButton } = this.state;

        let closedButton = null;

        if (isClosedButton) {
            closedButton = <button type="button" className="modal-closed" onClick={this.onClosedModalHandler}></button>
        }
        
        return (
            <div>
                <Loader text="Добавление транзакции" loading={loading}></Loader>
                <Modal
                    isOpen={this.props.open}
                    style={customStyle}
                    contentLabel="Modal"
                    >
                    {closedButton}
                    <h3>Добавление транзакции</h3>
                    <Input title="Сумма"
                        onChangeHandler={this.handler.bind(this, 'amount')}
                        value={transaction.amount}
                        disabled={disabledForm}
                        error={errors.amount} />
                    <Input title="Комментарий"
                        onChangeHandler={this.handler.bind(this, 'comment')}
                        value={transaction.comment}
                        disabled={disabledForm}
                        error={errors.comment} />
                    <span className="align-center">
                        <Button title={buttonText} onClickHandler={this.submit} disabled={disabledSubmit} />
                    </span>
                </Modal>
            </div>
        );
    }
}

SetTransaction.propTypes = {
    open: PropTypes.bool.isRequired,
    userID: PropTypes.string.isRequired,
    onClosedModal: PropTypes.func.isRequired
}

export default SetTransaction;