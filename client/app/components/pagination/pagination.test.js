import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Pagination from './pagination.jsx';

describe('Компонент Pagination', () => {
    const pagination = shallow(<Pagination page={0} totalItems={0} onChangePage={function(){}} />);

    it('Проверка есть ли кнопки', () => {
        expect(pagination.find('li')).to.have.length(5);
    });

    it('Добавление кнопок', () => {
        pagination.setProps({totalItems: 40});
        expect(pagination.find('li')).to.have.length(6);
    });
});