import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './pagination.css';

class Pagination extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...props,
            limitPage: 10,
            itemPerPage: 20,
            rangeStepName: '..'
        };
    }

    handler = (event) => {
        this.props.onChangeHandler(event.target.value);
    }

    getTotalPages(totalItems, itemsPerPage) {
        let totalPages = 1 > this.itemsPerPage ? 1 : Math.ceil(totalItems / itemsPerPage);
        return Math.max(totalPages || 0, 1);
    }

    getPaging(currentPage, totalPages) {
        let pages = [];
        let startPage = 1, endPage = totalPages;
        let isMaxSized = (this.state.limitPage && this.state.limitPage < totalPages);
        if (isMaxSized) {
            startPage = ((Math.ceil((currentPage + 1) / this.state.limitPage) - 1) * this.state.limitPage) + 1;
            endPage = Math.min(startPage + this.state.limitPage - 1, totalPages);
        }
        for (let number = startPage; number <= endPage; number++) {
            pages.push(number);
        }
        if (isMaxSized) {
            if (1 < startPage) {
                pages.unshift({
                    name: this.state.rangeStepName,
                    value: startPage - this.state.limitPage - 1
                });
            }
            if (endPage < totalPages) {
                pages.push({
                    name: this.state.rangeStepName,
                    value: endPage
                });
            }
        }

        // Добавляем кнопки вперед назад
        pages.unshift({
            name: '<',
            value: currentPage - 1,
            disabled: (0 === currentPage)
        });
        pages.push({
            name: '>',
            value: currentPage + 1,
            disabled: (totalPages === currentPage + 1)
        });
        pages.unshift({
            name: '<<',
            value: 0,
            disabled: (0 === currentPage)
        });
        pages.push({
            name: '>>',
            value: totalPages - 1,
            disabled: (totalPages === currentPage + 1)
        });
        
        return pages;
    }

    onChangePageHandler(item) {
        let page;
        if ('object' === typeof item) {
            page = item.value;
        } else {
            page = item - 1;
        }

        if (0 >= page) {
            page = 0;
        }

        console.log(page)

        this.state.onChangePage(page);
    }

    render() {
        const { page, totalItems } = this.props;
        const { itemPerPage } = this.state;
        let paging = this.getPaging(page, this.getTotalPages(totalItems, itemPerPage));

        return (
            <div className="paginator-container">
                <ul className="paginator">
                    {
                        paging.map((item, index) => {
                            let className = '';
                            if (item.disabled) className += ' disabled';
                            if (page === item - 1) className += ' active';
                            return(
                                <li className={className} key={index} onClick={this.onChangePageHandler.bind(this, item)}>
                                    <a>
                                        <span>{item.name || item}</span>
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

Pagination.propTypes = {
    page: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    totalItems: PropTypes.number.isRequired
}

export default Pagination;