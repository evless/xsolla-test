import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

export default function configureStore(initialState = {}) {
    let middleware = [thunk];
    if ('production' !== process.env.NODE_ENV) {
        let logger = createLogger();
        middleware = [...middleware, logger];
    }
    
    const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));
    return store;
}
