import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import moment from 'moment';

import Amount from '../../components/amount/amount.jsx';
import Loader from '../../components/loader/loader.jsx';
import SetTransaction from '../../components/setTransaction/setTransaction.jsx';

import 'react-datepicker/dist/react-datepicker.css';
import './userView.css';
import '../../../public/styles/table.css';
import '../../../public/styles/fields.css';

class UserView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            userID: props.params.userID,
            user: {},
            transactions: [],
            date: {
                start: moment().day(0),
                end: moment().add(1, 'day')
            },
            openModal: false
        }

        this.getUserInfo();
    }

    getUserInfo() {
        fetch(`https://livedemo.xsolla.com/fe/test-task/baev/users/${this.state.userID}`)
            .then(res => res.json())
            .then(user => this.setState({user}, this.getTransactions))
            .catch(err => this.setState({loading: false}));
    }

    getTransactions() {
        let { start, end } = this.state.date;

        start = moment(start).format('YYYY-MM-DD');
        end = moment(end).format('YYYY-MM-DD');

        fetch(`https://livedemo.xsolla.com/fe/test-task/baev/users/${this.state.userID}/transactions?datetime_from=${start}T00%3A00%3A00Z&datetime_to=${end}T00%3A00%3A00Z`)
            .then(res => res.json())
            .then(transactions => this.setState({transactions, loading: false}))
            .catch(err => this.setState({loading: false}));
    }

    onChangeDate = (type, date) => {
        this.setState({
            date: {
                ...this.state.date,
                [type]: date
            },
            loading: true
        }, this.getTransactions);
    }

    renderTransactions = (transactions) => {
        return(
            <table className="table">
                <thead>
                    <tr>
                        <th>Номер операции</th>
                        <th>Дата</th>
                        <th>Коммент</th>
                        <th>Размер</th>
                        <th>Баланс</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        transactions.map((item, index) => {
                            return(
                                <tr key={index}>
                                    <td>{item.operation_id}</td>
                                    <td>{moment(item.date).format('DD.MM.YYYY')}</td>
                                    <td>{item.comment}</td>
                                    <td>
                                        <Amount value={item.amount} />
                                    </td>
                                    <td>
                                        <Amount value={item.user_balance} />
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        )
    }

    /**
     * Метод открытия/закрытия модального окна для создания транзакции
     * 
     * @param {Boolean} success -> Передаем true, если нужно обновить список транзакций
     */
    toogleModal = (success) => {
        this.setState({
            openModal: !this.state.openModal
        }, () => {
            if (true === success) {
                this.setState({
                    loading: true
                }, this.getUserInfo)
            }
        })
    }

    render() {
        const { user, loading, transactions, userID, openModal } = this.state;

        return (
            <div>
                <Loader text="Загрузка пользователя" loading={loading}></Loader>
                <SetTransaction open={openModal} userID={userID} onClosedModal={this.toogleModal} />
                <div>
                    <div className="content__subtitle">Общая информация</div>
                    <table className="user-info-table">
                        <tbody>
                            <tr>
                                <th>Логин</th>
                                <td>{user.user_id}</td>
                            </tr>
                            <tr>
                                <th>Имя</th>
                                <td>{user.user_name}</td>
                            </tr>
                            <tr>
                                <th>Дополнительное поле</th>
                                <td>{user.user_custom}</td>
                            </tr>
                            <tr>
                                <th>E-mail</th>
                                <td>{user.email}</td>
                            </tr>
                            <tr>
                                <th>Баланс</th>
                                <td>
                                    <Amount value={user.balance} />
                                </td>
                            </tr>
                            <tr>
                                <th>Дата создания</th>
                                <td>{moment(user.register_date).format('DD.MM.YYYY')}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div className="content__subtitle">Операции пользователя</div>

                    <div className="form-field">
                        <label className="form-field__text">
                            Период:
                        </label>
                        <span className="datepicker-field">
                            <DatePicker
                                className="datepicker"
                                selected={this.state.date.start}
                                locale="ru"
                                dateFormat="DD.MM.YYYY"
                                onChange={this.onChangeDate.bind(this, 'start')} />
                        </span>
                        <span className="field-separator">
                            –
                        </span>
                        <span className="datepicker-field">
                            <DatePicker
                                className="datepicker"
                                selected={this.state.date.end}
                                locale="ru"
                                dateFormat="DD.MM.YYYY"
                                onChange={this.onChangeDate.bind(this, 'end')} />
                        </span>
                        <span className="fl-r">
                            <button className="btn" type="button" onClick={this.toogleModal}>
                                Добавить транзакцию
                            </button>
                        </span>
                    </div>
                    {
                        0 < transactions.length
                        ? this.renderTransactions(transactions)
                        : <div>За выбранный период транзакций нет.</div>
                    }
                </div>
            </div>
        )
    }
}

UserView.propTypes = {
    params: PropTypes.object.isRequired
}

export default UserView