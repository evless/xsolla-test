import React, { Component } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import Loader from '../../components/loader/loader.jsx';
import Input from '../../components/input/input.jsx';
import Button from '../../components/button/button.jsx';

import { user } from '../../constans/user.js';

import './userCreate.css';


class UserCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            disabledForm: false,
            disabledSubmit: true,
            buttonText: 'Зарегистрироваться',
            user,
            errors: {}
        }
    }

    /**
     * Проверка нужно или нет задизейблить кнопку.
     */
    checkDisabled() {
        let { user } = this.state;

        if (0 < user.user_name.length && 0 < user.email.length && 0 < user.user_custom.length && 0 < user.user_id.length) {
            this.setState({
                disabledSubmit: false
            })
        } else {
            this.setState({
                disabledSubmit: true
            })
        }
    }

    /**
     * 
     * Общий обработчик для всех инпутов.
     * 
     * @param {String} type -> Имя поле в объекте, куда нужно сложить данные
     * @param {String} value -> Новое значение в инпуте
     */
    handler(type, value) {
        this.setState({
            user: {
                ...this.state.user,
                [type]: value
            }
        }, this.checkDisabled)
    }

    /**
     * Отправка формы на создание пользователя
     */
    submit = () => {
        const options = {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(this.state.user)
        };

        // Блокируем все поля и показываем лоадер
        // Колбэком выполняем запрос
        this.setState({
            loading: true,
            disabledForm: true,
            errors: {}
        }, () => {
            fetch('https://livedemo.xsolla.com/fe/test-task/baev/users', options)
                .then(res => res.text())
                .then(res => {
                    // Код ошибки всегда 200, поэтому пришлось проверять длинну ответа
                    // Если пользователь создан успешно, то ответ приходит пустым
                    // Иначе там провал с валидацией
                    if (!res.length) {
                        // Убираем лоадер и меняем текст в кнопке
                        this.setState({
                            buttonText: 'Пользователь создан',
                            disabledSubmit: true,
                            loading: false
                        }, () => {
                            // Потом возвращаем всё в исходное положение
                            setTimeout(() => {
                                this.setState({
                                    user,
                                    buttonText: 'Зарегистрироваться',
                                    disabledForm: false
                                })
                            }, 3000)
                        })
                    } else {
                        // Выводим ошибки.
                        // Парсим строк в json и выдераем от туда объект с ошибками
                        // Но, если 409 статус, то логин занят и ошибка лежит в другом месте
                        let resJSON = JSON.parse(res);
                        let errors = resJSON.extended_message.property_errors;
                        if (409 === resJSON.http_status_code) {
                            errors = {
                                custom: resJSON.message
                            }
                        }

                        this.setState({
                            loading: false,
                            disabledForm: false,
                            errors 
                        })
                    }
                })
        })
    }

    render() {
        const { user, loading, disabledSubmit, buttonText, disabledForm, errors } = this.state;

        return (
            <div>
                <Loader text="Создание пользователя" loading={loading}></Loader>

                <Input title="Логин" onChangeHandler={this.handler.bind(this, 'user_id')} value={user.user_id} disabled={disabledForm} error={errors.user_id} />
                <Input title="Имя" onChangeHandler={this.handler.bind(this, 'user_name')} value={user.user_name} disabled={disabledForm} error={errors.user_name} />
                <Input title="Дополнительное поле" onChangeHandler={this.handler.bind(this, 'user_custom')} value={user.user_custom} disabled={disabledForm} error={errors.user_custom} />
                <Input title="E-mail" onChangeHandler={this.handler.bind(this, 'email')} value={user.email} disabled={disabledForm} error={errors.email} />
                {
                    errors.custom
                    ? <div className="form-field__error">{errors.custom}</div>
                    : null
                }

                <Button title={buttonText} onClickHandler={this.submit} disabled={disabledSubmit} />
            </div>
        )
    }
}

export default UserCreate