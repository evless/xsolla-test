import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './header.css';

class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header className="header">{this.props.title}</header>
        )
    }
}

Header.propTypes = {
    title: PropTypes.string.isRequired
}

export default Header;