import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Header from '../header/header.jsx';
import Sidebar from '../sidebar/sidebar.jsx';

import './content.css';

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    render() {
        return (
            <div>
                <Header title={this.props.children.props.route.title} />
                <Sidebar />
                
                <section className="content">
                    <div className="wrapper">
                        {this.props.children}
                    </div>
                </section>
            </div>
        )
    }
}

Body.propTypes = {
    children: PropTypes.object
}

export default Body;

