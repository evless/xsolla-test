import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './notFound.css';

class NotFound extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="not-found">
                <div className="not-found__title">{this.props.route.title}</div>
                <div className="not-found__image"></div>
            </div>
        )
    }
}

NotFound.propTypes = {
    route: PropTypes.object
}

export default NotFound;