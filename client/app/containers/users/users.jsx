import React, { Component } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import Loader from '../../components/loader/loader.jsx';
import Amount from '../../components/amount/amount.jsx';
import Pagination from '../../components/pagination/pagination.jsx';

import '../../../public/styles/table.css';

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            users: [],
            page: 0,
            totalItems: 0
        }

        this.getList();
    }

    getList() {
        let { page } = this.state;

        fetch(`https://livedemo.xsolla.com/fe/test-task/baev/users?offset=${page * 20}&limit=20`)
            .then(res => res.json())
            .then(res => this.setState({users: res.data, totalItems: res.recordsTotal, loading: false}))
            .catch(err => this.setState({loading: false}));
    }

    onChangePage = (page) => {
        this.setState({page, loading: true}, this.getList)
    }

    render() {
        const { users, loading } = this.state;

        return (
            <div>
                <Loader text="Загрузка пользователей" loading={loading}></Loader>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th className="align-right">Баланс</th>
                            <th className="align-right">Дата регистрации</th>
                        </tr>
                        {
                            users.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>
                                            <Link to={`/user/${item.user_id}/view`}>{item.user_id}</Link>
                                        </td>
                                        <td>
                                            <Link to={`/user/${item.user_id}/view`}>{item.user_name}</Link>
                                        </td>
                                        <td>{item.email}</td>
                                        <td className="align-right">
                                            <Amount value={item.balance} />
                                        </td>
                                        <td className="align-right">{moment(item.register_date).format('DD.MM.YYYY')}</td>
                                    </tr>
                                )
                            })
                        }
                    </thead>
                </table>
                <Pagination page={this.state.page} totalItems={this.state.totalItems} onChangePage={this.onChangePage} />
            </div>
        )
    }
}

export default Users