import React, { Component } from 'react';
import { Link } from 'react-router';

import './sidebar.css';

class Sidebar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <sidebar className="sidebar">
                <div className="logo">
                    Xsolla Test
                </div>
                <ul className="sidebar__list">
                    <li>
                        <Link to="/users" activeClassName="sidebar__item_status_active">Список пользователей</Link>
                    </li>
                    <li>
                        <Link to="/create" activeClassName="sidebar__item_status_active">Создание пользователя</Link>
                    </li>
                </ul>
            </sidebar>
        )
    }
}

export default Sidebar