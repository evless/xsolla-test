# Тестовое приложение для управления пользователями

## Используемые технологии
1. React (JS)
2. Redux (JS) [На самом деле не пригодился в текущем приложении]
3. PostCSS (CSS)
4. Webpack
5. Mocha

## Установка
1. NodeJs >= 6
2. npm install

## Запуск
1. npm run build:dev - Запуск в режиме разработки
2. npm run build:prod - Запуск в продакшн режиме
3. npm run test - Запуск тестов

## Ссылка
- https://xsolla-test.herokuapp.com